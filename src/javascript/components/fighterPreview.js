import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  
  const { _id ,name, health,attack,defense,} = fighter;
  
  const attributes = { 
    health: health, 
    title: name,
    attack:attack ,
    _id:_id,
    defense:defense,
    
  };
  const fighterElement = createElement({
    
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
    
    attributes:attributes
   
  });
  
  
 
  fighterElement.insertAdjacentHTML('afterbegin', `<ul><li>name ${name}</li><li>defense ${defense}</li><li>health ${health}</li><li>attack ${attack}</li></ul>`);
  
  // todo: show fighter info (image, name, health, etc.)
  
  return fighterElement;
  
}

export function createFighterImage(fighter) {
  
  const { source, name,} = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
