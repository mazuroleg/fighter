import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const attacker=firstFighter;
  const defender=secondFighter;
  //let resultDamage=getDamage(attacker, defender);
  
console.log(resultDamage)
  return new Promise((resolve) => {
    
    return resultDamage
    
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  
  let damage=getHitPower(attacker) - getBlockPower(defender);
  
  return damage
  
  // return damage
}

export function getHitPower(attacker) {

  //power = attack * criticalHitChance
  let power=attacker.attack*(Math.random()+1);
  
  return power
}

export function getBlockPower(defender) {
  
  //power = defense * dodgeChance
  let defense=defender.defense*(Math.random()+1)
  return defense
  // return block power
}
